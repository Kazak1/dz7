# 1
l1 = [i for i in range(10)]
l2 = [i for i in range(50,60)]
l = ['h','','e','l','','l','o','','!']


double = [*map(lambda x: x*2,l1)]
print(double)

x = [*map(lambda x,y,z: x*y*z,l1,l2)]
print(x)

length = [*map(lambda x: len(str(x)),x)]
print(length)

f1 = [*filter(lambda x: x%2==0,l1)]
print(f1)

f2 = [*filter(None,l)]
print(f2)

z1 = [*zip(l1,l2)]
print(z1)

z2 = [*zip(l1,map(lambda x: x*2,l2))]
print(z2)

